/**
 * This is template for main module created by New Kinetis SDK 2.x Project Wizard. Enjoy!
 **/

#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_gpio.h"
#include "fsl_kbi.h"
#include "fsl_port.h"
#include "fsl_clock.h"
#include "fsl_ftm.h"
#include "fsl_tpm.h"
#include "fsl_uart.h"
#include "fsl_debug_console.h"

//#include "slider_sm.h"
#include "par_gpio.h"
//#include "test.h"

#include <stdio.h>
#include <stdlib.h>

#define POLLING_TIME 300

typedef enum {
	POLLING_TIMEOUT,
	DISCHARGE_TIMEOUT
} state_t;

static uint8_t discharged_flag = 0;
static state_t tm_state = POLLING_TIMEOUT;

static uint32_t ticks_per_us = 0;

static gpio_pin_config_t pin_out = {
		.pinDirection = kGPIO_DigitalOutput,
		.outputLogic = 0
};

static gpio_pin_config_t pin_in = {
		.pinDirection = kGPIO_DigitalInput,
};

void FTM2_IRQHandler(void) {
	if (FTM_GetStatusFlags(FTM2) == kFTM_TimeOverflowFlag) {

		switch (tm_state) {
		case POLLING_TIMEOUT:
			FTM_StopTimer(FTM2);
			FTM2->CNT = 0;
			FTM_SetTimerPeriod(FTM2, 50 /* us */ * ticks_per_us);
			par_fgpio_init(kGPIO_PORTD, 1 << 6 | 1 << 7, &pin_out);
			FTM_StartTimer(FTM2, kFTM_SystemClock);
			tm_state = DISCHARGE_TIMEOUT;
			break;
		case DISCHARGE_TIMEOUT:
			FTM_StopTimer(FTM2);
			FTM2->CNT = 0;
			discharged_flag = 1;
			tm_state = POLLING_TIMEOUT;
			break;
		}
		FTM_ClearStatusFlags(FTM2, kFTM_TimeOverflowFlag);
	}
}

void send_to_plotter(uint32_t tv) {
	static uint16_t sof = 0xAABB;
	for (int i = 0; i < 2; i++) {
		DbgConsole_Putchar(*((char *)&sof + i));
	}
	for (int i = 0; i < 4; i++) {
		DbgConsole_Putchar(*((char *)&tv + i));
	}
}
void send_array_to_plotter(uint32_t tv[], uint32_t count) {
	static uint16_t sof = 0xAABB;
	for (int i = 0; i < 2; i++) {
		DbgConsole_Putchar(*((unsigned char *)&sof + i));
	}
	for (uint32_t i = 0; i < count; i++) {
		for (uint32_t c = 0; c < 4; c++) {
			DbgConsole_Putchar(*((unsigned char *)&tv[i] + c));
		}
	}
}

/*!
 * @brief Application entry point.
 */
int main(void) {
  /* Init board hardware. */
  BOARD_InitPins();
  BOARD_BootClockRUN();
  BOARD_InitDebugConsole();

  ticks_per_us = CLOCK_GetBusClkFreq() / 1000000;

  // Set polling timer
  NVIC_EnableIRQ(FTM2_IRQn);

  ftm_config_t ftm_config;
  FTM_GetDefaultConfig(&ftm_config);
  FTM_Init(FTM2, &ftm_config);

  FTM2->CNT = 0;
  FTM_EnableInterrupts(FTM2, kFTM_TimeOverflowInterruptEnable);
  FTM_SetTimerPeriod(FTM2, POLLING_TIME /* us */ * ticks_per_us);
  FTM_StartTimer(FTM2, kFTM_SystemClock);

  // Config pins as outputs
  par_fgpio_init(kGPIO_PORTD, 1 << 6 | 1 << 7, &pin_in);
  par_fgpio_enable_read(kGPIO_PORTD, 1 << 6 | 1 << 7, true);

  uint32_t time_value[2] = {0, 0};

  for(;;) { /* Infinite loop to avoid leaving the main function */
	  if (discharged_flag) {
		  FTM_DisableInterrupts(FTM2, kFTM_TimeOverflowInterruptEnable);
		  FTM_SetTimerPeriod(FTM2, 0xFFFFFFFF);
		  FTM_StartTimer(FTM2, kTPM_SystemClock);
		  par_fgpio_init(kGPIO_PORTD, 1 << 6 | 1 << 7, &pin_in);

		  /*
		   * 0b00 - no one
		   * 0b01 - PTD6
		   * 0b10 - PTD7
		   * 0b11 - both
		   */
		  uint32_t pin_mask = 1 << 6 | 1 << 7;
		  uint32_t pin_state = 0x0;
//		  uint32_t time_value[2] = {0, 0};
		  uint8_t run = 1;
		  while (run) {
			  pin_state = par_fgpio_read(kGPIO_PORTD, 1 << 6 | 1 << 7);
			  switch (pin_state & pin_mask) {
			  case 0x0:
				  if (pin_state != (1 << 6 | 1 << 7))
					  continue;

				  send_array_to_plotter(time_value, 2);

				  // Start polling
				  FTM_StopTimer(FTM2);
				  FTM2->CNT = 0;
				  FTM_EnableInterrupts(FTM2, kFTM_TimeOverflowInterruptEnable);
				  FTM_SetTimerPeriod(FTM2, POLLING_TIME /* us */ * ticks_per_us);
				  FTM_StartTimer(FTM2, kFTM_SystemClock);
				  run = false;
				  break;
			  case 1 << 6:
				  pin_mask &= ~(1 << 6);
			  	  FTM_StopTimer(FTM2);
			  	  time_value[0]++;
//				  time_value[0] = FTM2->CNT;
				  break;
			  case 1 << 7:
				  pin_mask &= ~(1 << 7);
			  	  time_value[1]++;
//				  time_value[1] = FTM2->CNT;
				  break;
			  case 1 << 6 | 1 << 7:
				  pin_mask = 0;
			  	  time_value[0]++;
			  	  time_value[1]++;
//				  time_value[0] = time_value[1] = FTM2->CNT;
				  break;
			  }
		  }

		  discharged_flag = 0;
	  }
  }
}
