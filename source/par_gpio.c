/*
 * par_gpio.c
 *
 *  Created on: Apr 3, 2018
 *      Author: eu
 */

#include "par_gpio.h"

#include "MKE02Z4.h"
#include "MKE02Z4_features.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define PORT_NUMBERS_EACH_GPIO   (4U)
#define PIN_NUMBERS_EACH_PORT (8U)
/*******************************************************************************
 * Variables
 ******************************************************************************/
#if defined(FSL_FEATURE_SOC_GPIO_COUNT) && FSL_FEATURE_SOC_GPIO_COUNT
static GPIO_Type *const s_gpioBases[] = GPIO_BASE_PTRS;
#endif

#if defined(FSL_FEATURE_SOC_FGPIO_COUNT) && FSL_FEATURE_SOC_FGPIO_COUNT
static FGPIO_Type *const s_fgpioBases[] = FGPIO_BASE_PTRS;
#if defined(FSL_FEATURE_PCC_HAS_FGPIO_CLOCK_GATE_CONTROL) && FSL_FEATURE_PCC_HAS_FGPIO_CLOCK_GATE_CONTROL

#if !(defined(FSL_SDK_DISABLE_DRIVER_CLOCK_CONTROL) && FSL_SDK_DISABLE_DRIVER_CLOCK_CONTROL)
/*! @brief Array to map FGPIO instance number to clock name. */
static const clock_ip_name_t s_fgpioClockName[] = FGPIO_CLOCKS;
#endif /* FSL_SDK_DISABLE_DRIVER_CLOCK_CONTROL */

#endif /* FSL_FEATURE_PCC_HAS_FGPIO_CLOCK_GATE_CONTROL */

#endif /* FSL_FEATURE_SOC_FGPIO_COUNT */

void par_fgpio_write(gpio_port_num_t port, uint8_t mask, uint8_t value);

void par_fgpio_init(gpio_port_num_t port, uint8_t mask, gpio_pin_config_t *config) {
    assert(config);

    uint8_t instance = (uint8_t)port / PORT_NUMBERS_EACH_GPIO;
    uint8_t shift = (uint8_t)port % PORT_NUMBERS_EACH_GPIO;
    FGPIO_Type *base = s_fgpioBases[instance];

    if (config->pinDirection == kGPIO_DigitalInput)
    {
        base->PDDR &= ~(mask << (shift * PIN_NUMBERS_EACH_PORT));
    }
    else
    {
        base->PDDR |= (mask << (shift * PIN_NUMBERS_EACH_PORT));
        par_fgpio_write(port, mask, config->outputLogic);
    }
}

void par_fgpio_write(gpio_port_num_t port, uint8_t mask, uint8_t value) {
	uint8_t instance = (uint8_t)port / PORT_NUMBERS_EACH_GPIO;
	uint8_t shift = (uint8_t)port % PORT_NUMBERS_EACH_GPIO;
	FGPIO_Type *base = s_fgpioBases[instance];

	if (value == 0U)
	{
		base->PCOR = mask << (shift * PIN_NUMBERS_EACH_PORT);
	}
	else
	{
		base->PSOR = mask << (shift * PIN_NUMBERS_EACH_PORT);
	}
}

uint32_t par_fgpio_read(gpio_port_num_t port, uint8_t mask) {
    uint8_t instance = (uint8_t)port / PORT_NUMBERS_EACH_GPIO;
    uint8_t shift = (uint8_t)port % PORT_NUMBERS_EACH_GPIO;
    FGPIO_Type *base = s_fgpioBases[instance];

    return (((base->PDIR) >> (shift * PIN_NUMBERS_EACH_PORT)) & mask);
}

void par_fgpio_enable_read(gpio_port_num_t port, uint8_t mask, bool enable) {
	uint8_t instance = (uint8_t)port / PORT_NUMBERS_EACH_GPIO;
	uint8_t shift = (uint8_t)port % PORT_NUMBERS_EACH_GPIO;
	FGPIO_Type *base = s_fgpioBases[instance];

	if (enable == false)
	{
		base->PIDR |= mask << (shift * PIN_NUMBERS_EACH_PORT);
	}
	else
	{
		base->PIDR &= ~(mask << (shift * PIN_NUMBERS_EACH_PORT));
	}
}


