#include "test.h"

#include "fsl_ftm.h"
#include "fsl_gpio.h"
#include "fsl_kbi.h"

static kbi_config_t kbi = {.mode = kKBI_EdgesLevelDetect, .pinsEnabled = 1 << 6, .pinsEdge = 1 << 6};

void kbi_test() {
	gpio_pin_config_t gpio;
	gpio.pinDirection = kGPIO_DigitalOutput;
	gpio.outputLogic = 0;
	GPIO_PinInit(kGPIO_PORTD, 6, &gpio);
	GPIO_PinWrite(kGPIO_PORTD, 6, 0);

	KBI_Init(KBI1, &kbi);
	KBI_EnableInterrupts(KBI1);
	NVIC_EnableIRQ(KBI1_IRQn);
}

//void KBI1_IRQHandler() {
//	if (KBI_IsInterruptRequestDetected(KBI1)) {
//		KBI_ClearInterruptFlag(KBI1);
//		__asm("NOP");
//	}
//}

void timer_test() {
	// FTM enable
	ftm_config_t ftm_config;
	FTM_GetDefaultConfig(&ftm_config);
	ftm_config.prescale = kFTM_Prescale_Divide_1;
	FTM_Init(FTM2, &ftm_config);
	FTM_SetTimerPeriod(FTM2, 0xFFFFFFFF);
}

void loop() {
	static uint8_t v = 1;
	if (v) {
		KBI_Deinit(KBI1);
	} else {
		KBI_Init(KBI1, &kbi);
	}
	v = !v;
}

void test_init() {
	kbi_test();
}
