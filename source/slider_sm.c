/*
 * slider_sm.c
 *
 *  Created on: Mar 7, 2018
 *      Author: eu
 */

#include <stdint.h>

#include "fsl_ftm.h"
#include "fsl_irq.h"
#include "fsl_kbi.h"
#include "fsl_gpio.h"


#if defined(TRACING_ENABLED)
typedef enum {
	DISCHARGE 		  	= 0x0,
	SET_POLLING_TIMER 	= 0x1,
	SET_DISCHARGE_TIMER	= 0x2,
	ENABLE_KBI			= 0x3,
	FTM2_IRQ_HANDLER	= 0x4,
	KBI1_IRQ_HANDLER	= 0x5
} trace_func_t;

typedef enum {
	FN_ENTER			= 0x0,
	FN_EXIT				= 0x1
} trace_event_t;

typedef struct {
	trace_func_t trf;
	trace_event_t tre;
} trace_rec_t;

static trace_rec_t rbf[1024] = {};
static int rbf_idx = 0;
static int rec_rbf_idx = 0;
static int trace_put(trace_func_t trf, trace_event_t tre) {
	rec_rbf_idx = rbf_idx;
	trace_rec_t tr = {.trf = trf, .tre = tre};
	rbf[rbf_idx] = tr;
	rbf_idx = (rbf_idx + 1) % (sizeof(rbf) / sizeof(rbf[0]));
	return rec_rbf_idx;
}
#else
#define trace_put(...)
#endif

#define POLLING_TIME   300 // us
#define DISCHARGE_TIME 50  // us
#define MAX_TIME       0xFFFFFFFF

static uint8_t timers_ready = 0;
static uint32_t timers[2] = {0, 0};
static uint32_t ticks_per_us = 0;

static uint32_t ticks(uint32_t time_in_us) {
	assert(ticks_per_us);
	return ticks_per_us * time_in_us;
}

typedef enum {
	DISCHARGE_1,
	CHARGE_1,
	READ_TIME_1,
	DISCHARGE_2,
	CHARGE_2,
	READ_TIME_2
} sm_state_t;

typedef enum {
	FTM_INTERRUPT,
	KBI_INTERRUPT
} event_t;

typedef enum {
	PIN_6 = 6,
	PIN_7 = 7
} pin_t;

static
void set_polling_timer() {
	FTM2->CNT = 0;
	FTM_SetTimerPeriod(FTM2, ticks(POLLING_TIME));
	FTM_StartTimer(FTM2, kFTM_SystemClock);
}

static
void handle_discharge(pin_t pin) {
	static gpio_pin_config_t config = {
			.pinDirection = kGPIO_DigitalOutput,
			.outputLogic = 0
	};

	FTM_StopTimer(FTM2);
	FTM2->CNT = 0;
	FTM_SetTimerPeriod(FTM2, ticks(DISCHARGE_TIME));
	FTM_StartTimer(FTM2, kFTM_SystemClock);

	GPIO_PinInit(kGPIO_PORTD, (uint8_t)pin, &config);
	GPIO_PinWrite(kGPIO_PORTD, (uint8_t)pin, 0);
}

static
void handle_charge(pin_t pin) {
	static kbi_config_t kbi_config = {
			.mode = kKBI_EdgesLevelDetect
	};
	kbi_config.pinsEdge = 1 << (uint8_t)pin;
	kbi_config.pinsEnabled = 1 << (uint8_t)pin;

	FTM_StopTimer(FTM2);
	FTM2->CNT = 0;
	FTM_SetTimerPeriod(FTM2, ticks(MAX_TIME));
	FTM_StartTimer(FTM2, kFTM_SystemClock);

	KBI_Init(KBI1, &kbi_config);
	KBI_EnableInterrupts(KBI1);
}

static
void handle_time(pin_t pin) {
	FTM_StopTimer(FTM2);
	timers[(uint8_t)pin % (sizeof(timers) / sizeof(timers[0]))] = FTM_GetCurrentTimerCount(FTM2);
}

static
void irq_state_machine(event_t event) {
	static sm_state_t states[] = {
			DISCHARGE_1,
			CHARGE_1,
			READ_TIME_1,
			DISCHARGE_2,
			CHARGE_2,
			READ_TIME_2
	};
	static uint8_t idx = 0;

begin_label:
	switch (states[idx]) {
	// First pin
	case DISCHARGE_1:
		handle_discharge(PIN_6);
		break;
	case CHARGE_1:
		handle_charge(PIN_6);
		break;
	case READ_TIME_1:
		switch (event) {
		case FTM_INTERRUPT:
			break;
		case KBI_INTERRUPT:
			handle_time(PIN_6);

			// Return to next pin
			idx++;
			goto begin_label;
		}
		break;

	// Second pin
	case DISCHARGE_2:
		handle_discharge(PIN_7);
		break;
	case CHARGE_2:
		handle_charge(PIN_7);
		break;
	case READ_TIME_2:
		switch (event) {
		case FTM_INTERRUPT:
			break;
		case KBI_INTERRUPT:
			handle_time(PIN_7);

			// Restart polling timer
			timers_ready = 1;
			set_polling_timer();
			break;
		}
		break;
	}

	idx = (idx + 1) % (sizeof(states) / sizeof(states[0]));
}

// State machine drivers
void FTM2_IRQHandler(void) {
	if (FTM_GetStatusFlags(FTM2) == kFTM_TimeOverflowFlag) {

		irq_state_machine(FTM_INTERRUPT);

		FTM_ClearStatusFlags(FTM2, kFTM_TimeOverflowFlag);
	}
}

void KBI1_IRQHandler(void) {
	if (KBI_IsInterruptRequestDetected(KBI1)) {

		irq_state_machine(KBI_INTERRUPT);

		KBI_DisableInterrupts(KBI1);
		KBI_ClearInterruptFlag(KBI1);
		KBI_Deinit(KBI1);
	}
}

void sm_init() {
	ftm_config_t config;
	FTM_GetDefaultConfig(&config);
	FTM_Init(FTM2, &config);
	FTM_EnableInterrupts(FTM2, kFTM_TimeOverflowInterruptEnable);
	NVIC_EnableIRQ(FTM2_IRQn);
	NVIC_EnableIRQ(KBI1_IRQn);

	ticks_per_us = CLOCK_GetBusClkFreq() / 1000000;
	set_polling_timer();
}

uint8_t sm_read_timers(uint32_t tm[2]) {
	uint8_t ret = 0;
	__disable_irq();
	ret = timers_ready;
	if (timers_ready) {
		tm[0] = timers[0];
		tm[1] = timers[1];
		timers_ready = 0;
	}
	__enable_irq();
	return ret;
}
