/*
 * par_gpio.h
 *
 *  Created on: Apr 3, 2018
 *      Author: eu
 */

#ifndef SOURCE_PAR_GPIO_H_
#define SOURCE_PAR_GPIO_H_

#include "fsl_gpio.h"

#include <stdbool.h>

// Fast parallel GPIO interface
void par_fgpio_init(gpio_port_num_t port, uint8_t mask, gpio_pin_config_t *config);
void par_fgpio_write(gpio_port_num_t port, uint8_t mask, uint8_t value);
uint32_t par_fgpio_read(gpio_port_num_t port, uint8_t mask);
void par_fgpio_enable_read(gpio_port_num_t port, uint8_t mask, bool enable);

#endif /* SOURCE_PAR_GPIO_H_ */
