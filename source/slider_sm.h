/*
 * slider_sm.h
 *
 *  Created on: Mar 7, 2018
 *      Author: eu
 */

#ifndef SOURCE_SLIDER_SM_H_
#define SOURCE_SLIDER_SM_H_

void sm_init(void);
uint8_t sm_read_timers(uint32_t timers[2]);

#endif /* SOURCE_SLIDER_SM_H_ */
